# Clicky Coming Soon

Serverless Coming Soon Pages.

Coming soon message with a contact form that sends the form data to
API Gateway which uses Lambda to process.

## Overview

- Requires multiple AWS services
- Version 1.1

## Requirements

- S3
- Lambda
- API Gateway
- SES
- DynamoDB

## Installation

- Copy `config-sample.js` to `config.js` and set values
- Copy `config-sample.json` to `config.json` and set value
- Configure S3 bucket for site hosting
  - Upload files into bucket and make public
- Create Lambda function to process data
- Create API Gateway to transport data
- Create SES user to email data
- Create DynamoDB to store data

Future documentation will include instructions on setting up the
required AWS services.
